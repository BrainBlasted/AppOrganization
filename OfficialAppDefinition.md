# Official GNOME App Definition

GNOME Core apps and GNOME Development tools are “Official GNOME Software” as defined by the [GNOME Foundation’s Software Policy](https://wiki.gnome.org/Foundation/SoftwarePolicy). This document defines the general idea behind those definitions. The detailed requirements are laid out in the [App Criteria](AppCriteria.md). Technical instructions for maintainers can be found in the [Module Requirements ](https://wiki.gnome.org/ReleasePlanning/ModuleRequirements).

## GNOME Core Apps

GNOME Core apps are part of the product delivered by the GNOME Project, and it is recommended that all Core apps are installed by default as part of the desktop.

GNOME's Core apps:

- provide functionality that is deemed to be essential for the desktop
- are designed and created by GNOME as a coherent suite
- are the result of close collaboration between maintainers, contributors, the Design Team, and Release Team
- provide a consistent and high quality user experience
- follow the GNOME Human Interface Guidelines
- are, when necessary, tightly integrated with the system
- have a generic name and identity.

In general, Core apps are still owned by their maintainers. In addition to their generic name, the also usually have a codename that identifies the project.

## GNOME Development Tools

GNOME Development tools are tailored to build and design apps for the GNOME ecosystem. They often integrate design policies or concepts specific to the GNOME ecosystem, enabling the implementation of those policies and concepts. They are also are often developed in conjunction with the GNOME Project’s onboarding strategy.

The GNOME Project does not recommend installing Development tools by default.
