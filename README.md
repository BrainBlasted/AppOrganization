# App Organization

This repository documents the processes and criteria for official GNOME apps and Circle apps. It covers the steps and processes for apps to become official GNOME apps and Circle apps, as well as how they are removed. These arrangements represent the practical implementation of the [GNOME Foundation's Software Policy](https://wiki.gnome.org/Foundation/SoftwarePolicy).

The repository contains the following:

| Document | Description |
|----------|-------------|
| [Official App Processes](AppLifecycle.md) | Describes the steps involved in adding and removing apps from the official app sets (including the incubation process) |
| [App Criteria](AppCriteria.md) | Lists of criteria which are used when deciding whether apps are suited for core, development or Circle |
| [Review Procedures](ReviewProcedures.md) | Reference material for app reviewers |
| [Official GNOME App Definition](OfficialAppDefinition.md) | Reference definition of what the GNOME core and development apps are expected to be like |

This repository is maintained by the GNOME [Release Team](https://gitlab.gnome.org/Teams/Releng) and [Circle Committee](https://gitlab.gnome.org/Teams/Circle/).

## Other Resources

- [Submission to GNOME Circle](https://gitlab.gnome.org/Teams/Circle)
- [Submission to Incubator](https://gitlab.gnome.org/Incubator/Submission) (GNOME Core apps and GNOME Development tools)
- [GNOME Build Metadata](https://gitlab.gnome.org/GNOME/gnome-build-meta)